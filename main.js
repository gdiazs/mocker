const mockserver = require('mockserver-node');
const mockServerClient = require("mockserver-client").mockServerClient;
const expectations = require("./src/expectations");

mockserver.start_mockserver({
    serverPort: 1080,
    verbose: true,
    trace: false
}).then(() => {
    expectations.forEach( expec => {
        mockServerClient("localhost", 1080).mockAnyResponse(expec).then(
            function () {
                console.log(expec,"expectations created");
            },
            function (error) {
                console.error(error)
            }
        );
    })
});

