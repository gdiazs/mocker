FROM node:lts-alpine3.14


RUN mkdir mocker
ADD src ./mocker/src
ADD main.js ./mocker
ADD package.json ./mocker
ADD package-lock.json ./mocker

RUN apk add --no-cache python3 g++ make openjdk11

WORKDIR /mocker
RUN npm install
CMD npm start

