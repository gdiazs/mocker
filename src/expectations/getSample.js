module.exports = {
    "httpRequest": {
        "method": "GET",
        "path": "/ejemplo"
    },
    "httpResponse": {
        "statusCode": 200,
        "headers": {
            "Location": [
                "https://www.mock-server.com"
            ]
        },
        "body": "lol world!"
    }
}