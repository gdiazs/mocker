module.exports = {
    "httpRequest": {
        "method": "GET",
        "path": "/test"
    },
    "httpResponse": {
        "statusCode": 200,
        "headers": {
            "Location": [
                "https://www.mock-server.com"
            ]
        },
        "body": "is test"
    }
}