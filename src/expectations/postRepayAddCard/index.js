const reponseBody = require("./reponse.json")

module.exports = {
    "httpRequest": {
        "method": "POST",
        "path": "/checkout-forms/token-payment"
    },
    "httpResponse": {
        "statusCode": 200,
        "headers": {
            "Location": [
                "https://www.mock-server.com"
            ]
        },
        "body": reponseBody
    }
}