const sample = require("./getSample")
const test = require("./getAnotherExample");
const postRepayAddCard = require("./postRepayAddCard");

module.exports = [
    sample,
    test,
    postRepayAddCard
]